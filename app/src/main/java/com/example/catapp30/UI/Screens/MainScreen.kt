package com.example.catapp30.UI.Screens

import android.content.Intent
import android.widget.Toast
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowDropDown
import androidx.compose.material.icons.filled.ExpandLess
import androidx.compose.material.icons.filled.ExpandMore
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.catapp30.CatsActivity
import com.example.catapp30.DetailActivity
import com.example.catapp30.R
import com.example.catapp30.Model.InfoCat
import com.example.catapp30.UI.Theme.Typography
import com.example.catapp30.UI.Theme.MyAppTheme
import com.skydoves.landscapist.ImageOptions
import com.skydoves.landscapist.glide.GlideImage
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import org.w3c.dom.Text

@Composable
fun MainScreen(
    UiState: UIState,
    setSelectCountry: (String) -> Unit,
    modifier: Modifier = Modifier
) {
    when (UiState) {
        is UIState.Loading -> LoadingScreen(modifier)
        is UIState.Success -> ResultScreen(
            UiState.photos,
            UiState.SelectCountry,
            setSelectCountry,
        )
        is UIState.Error -> ErrorScreen(modifier)
    }

}

@Composable
fun LoadingScreen(modifier: Modifier = Modifier) {
    Box(
        contentAlignment = Alignment.Center,
        modifier = modifier.fillMaxSize()
    ) {
        Image(
            modifier = Modifier.size(200.dp),
            painter = painterResource(R.drawable.gatocheto),
            contentDescription = stringResource(R.string.loading)
        )
    }
}

/**
 * The home screen displaying error message
 */
@Composable
fun ErrorScreen(modifier: Modifier = Modifier) {
    Column(
        modifier = modifier
            .background(color = Color.Red),
        horizontalAlignment= Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        Text(stringResource(R.string.loading_failed))
        GlideImage(
            imageModel = { R.drawable.xd }, modifier = Modifier
                .fillMaxWidth() .height(220.dp)
            ,
            imageOptions = ImageOptions(contentScale=ContentScale.Crop,contentDescription = stringResource(R.string.loading_failed))
        )
    }
}

@Composable
fun ResultScreen(
    catInfoList: List<InfoCat>,
    selectCountry: String,
    setSelectCountry: (String) -> Unit,

    ) {
    Column {
        var catInfoListFiltered = catInfoList
        DropDownComponent(selectCountry, setSelectCountry)
        if (selectCountry != "SelectCountry") {
            catInfoListFiltered = catInfoList.filter { it.countryCode == selectCountry }
        }
        Spacer(modifier = Modifier.height(15.dp))
        CatsList(catInfoListFiltered)
    }

}




@Composable
fun DropDownComponent(selectCountry: String, setSelectCountry: (String) -> Unit) {
    val listOfCountry =
        arrayOf(
            "SelectCountry",
            "EG",
            "US",
            "CA",
            "ES",
            "BR",
            "GR",
            "AE",
            "AU",
            "FR",
            "GB",
            "MM",
            "CY",
            "RU",
            "CN",
            "JP",
            "TH",
            "IM",
            "NO",
            "IR",
            "SP",
            "SO",
            "TR"
        )
    val contextForToast = LocalContext.current.applicationContext

    var expanded by remember {
        mutableStateOf(false)
    }

    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(5.dp),
        verticalAlignment = Alignment.Top,
        horizontalArrangement = Arrangement.Start
    ) {
        Button(onClick = {
            expanded = true
        }, shape = MaterialTheme.shapes.large) {
            Text(text = selectCountry)
            Icon(
                imageVector = Icons.Default.ArrowDropDown,
                contentDescription = "OpenOptions",
            )
        }
        DropdownMenu(
            expanded = expanded,
            onDismissRequest = {
                expanded = false
            },
            modifier = Modifier
                .height(250.dp)
                .background(color = MaterialTheme.colors.primary)
        ) {
            listOfCountry.forEach { countryCode ->
                DropdownMenuItem(
                    onClick = {
                        setSelectCountry(countryCode)
                        Toast.makeText(contextForToast, "Country: $countryCode", Toast.LENGTH_SHORT)
                            .show()
                        expanded = false
                    },
                    enabled = (countryCode != selectCountry)
                ) {
                    Text(text = countryCode, color = MaterialTheme.colors.primaryVariant)
                }
            }
        }
    }
}


@Composable
fun CatsList(catsList: List<InfoCat>, modifier: Modifier = Modifier) {
    val mContext = LocalContext.current
    LazyColumn(modifier = modifier) {
        items(items = catsList) { InfoCat ->
            Button(
                onClick = {
                    val intent = Intent(mContext, DetailActivity::class.java)
                    val catInfoStr = Json.encodeToString(InfoCat)
                    intent.putExtra("CatInfo", catInfoStr)
                    mContext.startActivity(intent)
                },
            ) {

                CatCard(
                    catBreed = InfoCat,
                )
            }
        }
    }
}


@Composable
fun CatCard(catBreed: InfoCat, modifier: Modifier = Modifier) {

    Card(
        modifier = modifier,
        border = BorderStroke(
            1.dp,
            color = MaterialTheme.colors.primary
        ),
        elevation = 5.dp,
        backgroundColor = MaterialTheme.colors.background
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(5.dp),
            horizontalArrangement = Arrangement.Start,
            verticalAlignment = Alignment.CenterVertically,

            ) {
            GlideImage(
                imageModel = { catBreed.image!!.url }, modifier = Modifier
                    .size(200.dp),
                imageOptions = ImageOptions(contentScale = ContentScale.Crop)
            )

            Text(
                fontSize = 20.sp,
                text = "Breed:\n" + catBreed.name,
                color = MaterialTheme.colors.secondary,
                modifier = Modifier.padding(start = 35.dp),
                textAlign = TextAlign.Center,
            )
        }
    }
}

