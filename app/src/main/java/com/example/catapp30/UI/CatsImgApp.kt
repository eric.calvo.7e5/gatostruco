package com.example.catapp30.UI

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.catapp30.UI.Screens.MainScreen
import com.example.catapp30.R
import com.example.catapp30.UI.Screens.ViewModel


@Composable
fun CatsImgApp(modifier: Modifier = Modifier) {
    Scaffold(
        modifier = modifier.fillMaxSize(),
        topBar = { TopAppBar(title= {Text(stringResource(R.string.app_name)) })}
    ) {
        Surface(
            modifier = Modifier
                .fillMaxSize()
                .padding(it),
            color = MaterialTheme.colors.background
        ) {
            val catsViewModel: ViewModel = viewModel()
            MainScreen(UiState = catsViewModel.UiState,
                setSelectCountry = {catsViewModel.SetSelCountry(it)},
                modifier = Modifier.background(color = MaterialTheme.colors.primaryVariant)
            )

        }
    }
}


