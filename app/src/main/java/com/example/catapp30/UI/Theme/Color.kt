package com.example.catapp30.UI.Theme

import androidx.compose.ui.graphics.Color

val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF3700B3)
val Teal200 = Color(0xFF03DAC5)
val Orange500 = Color(0xFFFF9800)
val Yellow500 = Color(0xFFFFC107)
val Red500 = Color(0xFFF44336)
val Black = Color(0xFF000000)
val White = Color(0xFFFFFFFF)
val DarkBrown = Color(0xFF331305)
val LightBrown = Color(0xFF825C4F)
