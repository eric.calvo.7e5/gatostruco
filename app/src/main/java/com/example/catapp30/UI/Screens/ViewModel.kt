package com.example.catapp30.UI.Screens

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.catapp30.Network.ApiService
import com.example.catapp30.Model.InfoCat
import com.example.catapp30.Network.Api
import kotlinx.coroutines.launch
import retrofit2.HttpException
import java.io.IOException

sealed interface UIState{
    data class Success(val photos:List<InfoCat>, val SelectCountry: String): UIState
    object Error: UIState
    object Loading: UIState
}


class ViewModel: ViewModel() {
    var UiState: UIState by mutableStateOf(UIState.Loading)
        private set

    init{
        getPhotos()
    }

    fun getPhotos(){
        viewModelScope.launch {
            UiState=try {
                val listResult = Api.retrofitService.getInfo()
                UIState.Success(listResult, "SelectCountry")
            } catch (e: IOException){
                UIState.Error
            } catch (e: HttpException){
                UIState.Error
            }
        }
    }

    fun SetSelCountry(newCountry: String){
        if(UiState is UIState.Success){
            UiState=(UiState as UIState.Success).copy(SelectCountry = newCountry)
        }
    }
}
