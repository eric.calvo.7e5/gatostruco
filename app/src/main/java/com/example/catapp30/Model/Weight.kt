package com.example.catapp30.Model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable


@Serializable
data class Weight(
    @SerialName("imperial") var imperial: String? = null,
    @SerialName("metric") var metric: String? = null

)


