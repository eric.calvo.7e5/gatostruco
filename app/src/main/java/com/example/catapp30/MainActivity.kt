package com.example.catapp30

import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.MaterialTheme.colors
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Visibility
import androidx.compose.material.icons.filled.VisibilityOff
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusManager
import androidx.compose.ui.graphics.Color.Companion.Black
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.*
import androidx.compose.ui.unit.dp
import com.example.catapp30.UI.Theme.MyAppTheme



class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MyAppTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = colors.background
                )
                {
                    ShowLogIn()
                }


            }
        }
    }
}

@Composable
fun ShowLogIn() {

    Column(
        modifier = Modifier
            .fillMaxWidth()
            .background(color = colors.primary),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally,

        ) {
        Image(
            modifier = Modifier.padding(top = 20.dp),
            painter = painterResource(id = R.drawable.gigachadcat),
            contentDescription = stringResource(id = R.string.app_name),
            contentScale = ContentScale.Fit,
        )
        Text(
            modifier = Modifier.padding(top = 10.dp),
            text = "YouSir",
        )

        val focusManager = LocalFocusManager.current

        EmailInput(focusManager)
        Text(text = "Password")
        PasswordInput(focusManager)
        LogInButton()


    }

}

@Composable
fun EmailInput(focusManager: FocusManager) {

    var emailText by remember { mutableStateOf(TextFieldValue("")) }
    OutlinedTextField(
        value = emailText,
        onValueChange = { emailText = it },
        colors = TextFieldDefaults.outlinedTextFieldColors(
            focusedBorderColor = colors.primaryVariant,
            textColor = Black
        ),
        label = { Text("example@example.com", color = colors.secondary) },
        singleLine = true,
        modifier = Modifier.fillMaxWidth(),
        keyboardActions = KeyboardActions(onDone = { focusManager.clearFocus() }),
        keyboardOptions = KeyboardOptions.Default.copy(
            imeAction = ImeAction.Next,
            keyboardType = KeyboardType.Email
        ), isError= android.util.Patterns.EMAIL_ADDRESS.matcher(emailText.text).matches()

    )
}

@Composable
fun PasswordInput(focusManager: FocusManager) {
    var psswrd by remember { mutableStateOf(TextFieldValue("")) }
    var psswrdOnView by remember { mutableStateOf(false) }

    OutlinedTextField(

        value = psswrd,
        onValueChange = { psswrd = it },
        label = { Text("******",color = colors.secondary) },
        colors = TextFieldDefaults.outlinedTextFieldColors(
            focusedBorderColor = colors.primaryVariant,
            textColor = Black
        ),
        singleLine = true,
        modifier = Modifier.fillMaxWidth(),
        keyboardActions = KeyboardActions(onDone = { focusManager.clearFocus() }),
        keyboardOptions = KeyboardOptions.Default.copy(
            imeAction = ImeAction.Done,
            keyboardType = KeyboardType.Password
        ),
        visualTransformation = if (psswrdOnView) VisualTransformation.None else PasswordVisualTransformation(),
        trailingIcon = {
            var image =
                if (psswrdOnView) Icons.Filled.Visibility else Icons.Filled.VisibilityOff
            val description = if (psswrdOnView) "Hide password" else "Show Password"
            IconButton(onClick = { psswrdOnView = !psswrdOnView }) {
                Icon(imageVector = image, description)
            }
        }
    )
}

@Composable
fun LogInButton() {
    val context = LocalContext.current

    Button(
        colors = ButtonDefaults.buttonColors(
            backgroundColor = colors.secondary,
            contentColor = colors.background
        ),
        onClick = {
            val intent = Intent(context, CatsActivity::class.java)
            context.startActivity(intent)
        }, modifier = Modifier.padding(top = 30.dp)
    ) {
        Text(text = "Log in")
    }
}


@Composable
fun DefaultPreview() {
    MyAppTheme {
        ShowLogIn()
    }
}
