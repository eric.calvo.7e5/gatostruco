package com.example.catapp30.Network


import com.example.catapp30.Model.InfoCat
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import kotlinx.serialization.json.Json
import okhttp3.MediaType.Companion.toMediaType
import retrofit2.Retrofit
import retrofit2.http.GET
import retrofit2.http.Headers


private const val BASE_URL = "https://api.thecatapi.com/v1/"
private const val token = "live_0j6JrgZRiGZnuIA9YKPMxKFOTYhWGp1kwiUtFWgbSdyj3jM6nA5jQnp4GW7OsTRq"
private val jsonIgnored = Json { ignoreUnknownKeys = true }
private val retrofit = Retrofit.Builder()
    .addConverterFactory(jsonIgnored.asConverterFactory("application/json".toMediaType()))
    .baseUrl(BASE_URL)
    .build()
interface ApiService {
    @Headers("x-api-key: $token")
    @GET("breeds")
    suspend fun getInfo(): List<InfoCat>
}
object Api {
    val retrofitService: ApiService by lazy {
        retrofit.create(ApiService::class.java)
    }
}
