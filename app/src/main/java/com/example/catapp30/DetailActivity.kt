package com.example.catapp30

import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.annotation.RequiresApi
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.catapp30.Model.InfoCat
import com.example.catapp30.UI.Theme.MyAppTheme
import com.skydoves.landscapist.ImageOptions
import com.skydoves.landscapist.glide.GlideImage
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json



class DetailActivity : ComponentActivity() {
    @RequiresApi(Build.VERSION_CODES.TIRAMISU)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val intent = intent
        val infoStr = intent.getStringExtra("Info")
        val info = infoStr?.let { Json.decodeFromString<InfoCat>(it) }

        setContent {
            MyAppTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(), color = MaterialTheme.colors.primary
                ) {
                    Column(
                        modifier = Modifier
                            .fillMaxSize()
                            .padding(15.dp)
                            .border(2.dp, MaterialTheme.colors.primaryVariant)
                            .background(color = MaterialTheme.colors.background)
                            .verticalScroll(
                                rememberScrollState()
                            ),
                        verticalArrangement = Arrangement.Top,
                        horizontalAlignment = Alignment.CenterHorizontally,

                        ) {
                        if (info != null) {
                            FullCatCard(info)
                        }
                    }
                }


            }
        }
    }

}


@Composable
fun FullCatCard(Info: InfoCat) {
    GlideImage(
        imageModel = { Info.image!!.url },
        modifier = Modifier
            .padding(5.dp)
            .height(270.dp)
            .fillMaxWidth(),
        imageOptions = ImageOptions(contentScale = ContentScale.Crop)
    )
    Description(Info)

}

@Composable
fun Description(Info: InfoCat) {

    Card(
        modifier = Modifier.padding(15.dp),
        border = BorderStroke(1.dp, color = MaterialTheme.colors.primary),
        elevation = 5.dp,
        backgroundColor = MaterialTheme.colors.onBackground
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(5.dp),
            horizontalArrangement = Arrangement.Start,
            verticalAlignment = Alignment.CenterVertically,

            ) {

            Text(
                text = "Breed: ", color = MaterialTheme.colors.primary
            )
            Info.name?.let {
                Text(
                    text = it,
                    color = MaterialTheme.colors.primaryVariant,
                    modifier = Modifier.padding(start = 15.dp, end = 15.dp),
                    fontSize = 11.sp,
                    textAlign = TextAlign.Center
                )
            }


        }
    }
    // Description

    Card(
        modifier = Modifier.padding(15.dp),
        border = BorderStroke(1.dp, color = MaterialTheme.colors.primary),
        elevation = 5.dp,
        backgroundColor = MaterialTheme.colors.onBackground
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(5.dp),
            horizontalArrangement = Arrangement.Start,
            verticalAlignment = Alignment.CenterVertically,

            ) {

            Text(
                text = "Description: ", color = MaterialTheme.colors.primary, modifier = Modifier
            )
            Info.description?.let {
                Text(
                    text = it,
                    color = MaterialTheme.colors.primaryVariant,
                    modifier = Modifier.padding(start = 15.dp, end = 15.dp),
                    fontSize = 11.sp,
                    textAlign = TextAlign.Justify
                )
            }


        }
    }
    // Temperament
    Card(
        modifier = Modifier.padding(15.dp),
        border = BorderStroke(1.dp, color = MaterialTheme.colors.primary),
        elevation = 5.dp,
        backgroundColor = MaterialTheme.colors.onBackground
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(5.dp),
            horizontalArrangement = Arrangement.Start,
            verticalAlignment = Alignment.CenterVertically,

            ) {

            Text(
                text = "Temperament: ", color = MaterialTheme.colors.primary, modifier = Modifier
            )
            Info.temperament?.let {
                Text(
                    text = it,
                    color = MaterialTheme.colors.primaryVariant,
                    modifier = Modifier.padding(start = 15.dp, end = 15.dp),
                    fontSize = 11.sp,
                    textAlign = TextAlign.Justify
                )
            }


        }
    }
    // Wiki
    Card(
        modifier = Modifier.padding(15.dp),
        border = BorderStroke(1.dp, color = MaterialTheme.colors.primary),
        elevation = 5.dp,
        backgroundColor = MaterialTheme.colors.onBackground
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(5.dp),
            horizontalArrangement = Arrangement.Start,
            verticalAlignment = Alignment.CenterVertically,

            ) {

            Text(
                text = "Wiki: ", color = MaterialTheme.colors.primary, modifier = Modifier
            )
            Info.wikipediaUrl?.let {
                MyButton(Info.wikipediaUrl!!)
            }


        }
    }
}

@Composable
fun MyButton(Url: String) {
    val context = LocalContext.current
    val intent = remember {
        Intent(Intent.ACTION_VIEW, Uri.parse(Url))
    }
    Box(
        modifier = Modifier.clickable { context.startActivity(intent) },
    ) {
        Text(
            text = Url,
            color = MaterialTheme.colors.primaryVariant,
            fontSize = 11.sp,
            textAlign = TextAlign.Center,
            modifier = Modifier.padding(start = 15.dp, end = 15.dp)
        )
    }
}


@Composable
fun DefaultPreview2() {
    MyAppTheme {}
}
